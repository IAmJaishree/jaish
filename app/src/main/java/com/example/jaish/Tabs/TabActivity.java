package com.example.jaish.Tabs;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.example.jaish.R;
import com.google.android.material.tabs.TabLayout;

public class TabActivity extends AppCompatActivity {


    ViewPager viewPager;

    TabLayout tabLayout;

    ViewPagerADapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);


//        getIntent().getExtras().get("keyName");
        viewPager = findViewById(R.id.view_pager);
        tabLayout= findViewById(R.id.tabs);
        adapter = new ViewPagerADapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }
}