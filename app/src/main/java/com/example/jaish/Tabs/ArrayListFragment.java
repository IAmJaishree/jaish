package com.example.jaish.Tabs;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.jaish.Product;
import com.example.jaish.ProductsAdapter;
import com.example.jaish.R;
import com.example.jaish.SecondActivity;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public  class ArrayListFragment extends Fragment {

    RecyclerView recyclerView;
    ProductsAdapter productsAdapter;
    int mNum;

    /**
     * Create a new instance of CountingFragment, providing "num"
     * as an argument.
     */
    static ArrayListFragment newInstance(int num) {
        ArrayListFragment f = new ArrayListFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("num", num);
        f.setArguments(args);

        return f;
    }


    /**
     * The Fragment's UI is just a simple text view showing its
     * instance number.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mNum = getArguments() != null ? getArguments().getInt("num") : 1;

        View v = inflater.inflate(R.layout.fragment_pager_list, container, false);
        View tv = v.findViewById(R.id.text);
        ((TextView)tv).setText("Fragment #" + mNum);
        return v;
    }

    @Override
    public void onViewCreated( View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ArrayList<Product> products = new ArrayList<>();

        products.add(new Product("rice","It's rice"));
        products.add(new Product("ricasdfae","It's rice"));
        products.add(new Product("asdfrice","It's asdfrice"));
        products.add(new Product("grrgrrice","Iasdfst's rice"));
        products.add(new Product("grsdrice","It's rice"));
        products.add(new Product("geerice","It's rice"));
        products.add(new Product("rice","It's rice"));
        products.add(new Product("ricasdfae","It's rice"));

        productsAdapter = new ProductsAdapter(products, getContext());
        recyclerView = view.findViewById(R.id.recyclerView);
        if(mNum != 0)
            recyclerView.setVisibility(View.GONE);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(productsAdapter);



    }


}