package com.example.jaish;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.jaish.Tabs.TabActivity;

public class MainActivity extends AppCompatActivity {

    Button secondButton;
    Button thirdButton;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.product_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.file:
                Toast.makeText(MainActivity.this, "asda",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.filwwe:
                Toast.makeText(MainActivity.this, "aasdasda",Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        secondButton= findViewById(R.id.secondButton);
        thirdButton= findViewById(R.id.thirdButton);


        thirdButton.setOnClickListener(view->{
            Intent intent = new Intent(MainActivity.this, TabActivity.class);

            intent.putExtra("keyName",4);
            startActivity(intent);
        });


        secondButton.setOnClickListener(view->{
            Intent intent = new Intent(MainActivity.this, SecondActivity.class);
            startActivity(intent);
        });




    }
}