package com.example.jaish;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;

import java.util.ArrayList;

public class SecondActivity extends AppCompatActivity {


    RecyclerView recyclerView;
    ProductsAdapter productsAdapter;
    EditText search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        search = findViewById(R.id.search);

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String query = charSequence.toString();

                productsAdapter.filter(query);



            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        ArrayList<Product> products = new ArrayList<>();

        products.add(new Product("rice","It's rice"));
        products.add(new Product("ricasdfae","It's rice"));
        products.add(new Product("asdfrice","It's asdfrice"));
        products.add(new Product("grrgrrice","Iasdfst's rice"));
        products.add(new Product("grsdrice","It's rice"));
        products.add(new Product("geerice","It's rice"));
        products.add(new Product("rice","It's rice"));
        products.add(new Product("ricasdfae","It's rice"));
        products.add(new Product("asdfrice","It's asdfrice"));
        products.add(new Product("grrgrrice","Iasdfst's rice"));
        products.add(new Product("grsdrice","It's rice"));
        products.add(new Product("geerice","It's rice"));
        products.add(new Product("rice","It's rice"));
        products.add(new Product("ricasdfae","It's rice"));
        products.add(new Product("asdfrice","It's asdfrice"));
        products.add(new Product("grrgrrice","Iasdfst's rice"));
        products.add(new Product("grsdrice","It's rice"));
        products.add(new Product("geerice","It's rice"));

        productsAdapter = new ProductsAdapter(products,SecondActivity.this);
        recyclerView = findViewById(R.id.recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(SecondActivity.this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(productsAdapter);

        Log.e("SEcond", "ONCreate");

    }


    @Override
    protected void onPause() {
        super.onPause();Log.e("SEcond", "onPAuse");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("SEcond", "onResume");
    }


    @Override
    protected void onStart() {
        super.onStart();

        Log.e("SEcond", "onSTart");
    }
}