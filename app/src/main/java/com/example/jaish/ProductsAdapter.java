package com.example.jaish;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

public class ProductsAdapter extends RecyclerView.Adapter {

    private Context context;
    private ArrayList<Product> products;
    private ArrayList<Product> filtered;
    private boolean isToSelect = false;



    public ProductsAdapter(ArrayList<Product> products, Context context) {
        this.products = products;
        this.filtered = products;

        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;

        if(viewType==0){
            itemView = LayoutInflater.from(context).inflate(R.layout.shop , parent, false);
            return new ShopsHolder(itemView);
        }else{
            itemView = LayoutInflater.from(context).inflate(R.layout.product , parent, false);
            return new ProductsHolder(itemView);
        }


    }

    @Override
    public int getItemViewType(int position) {

        if(position%2==0)return 0;
        return 1;

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if(holder instanceof ShopsHolder){
            ((ShopsHolder) holder).bind(position);
        }
        else
        ((ProductsHolder) holder).bind(position);
    }






    @Override
    public int getItemCount() {
        return filtered.size();
    }

    public void filter(String query) {

        filtered = new ArrayList<>();

        if(query.isEmpty())filtered = products;
        for (Product p :
                products) {

            if(p.getName().toLowerCase().contains(query.toLowerCase()))filtered.add(p);
        }


        notifyDataSetChanged();
    }


    public class ShopsHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView description;

        public ShopsHolder(View v) {
            super(v);


            name = v.findViewById(R.id.name);
            description = v.findViewById(R.id.description);
        }

        @SuppressLint("SetTextI18n")
        void bind(int listIndex) {

            Product product = filtered.get(listIndex);

            name.setText(product.name);
            description.setText(product.description);

        }
    }


    public class ProductsHolder extends RecyclerView.ViewHolder {
       TextView name;
       TextView description;

        public ProductsHolder(View v) {
            super(v);


            name = v.findViewById(R.id.name);
            description = v.findViewById(R.id.description);
        }

        @SuppressLint("SetTextI18n")
        void bind(int listIndex) {

            Product product = filtered.get(listIndex);

            name.setText(product.name);
            description.setText(product.description);

        }
    }



}

